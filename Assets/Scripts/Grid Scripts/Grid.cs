﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//README:
//Don't be confused by my use of .x and .y rather than .x and .z as I used a Vector 2 to input the gridsize
//In reality I am using the x and z axis and not touching the y axis at all really
public class Grid : MonoBehaviour {
	public LayerMask unwalkableMask;
	public Vector2 gridWorldSize;
	public float nodeRadius;
	public GridNode[,] grid;
	Vector3 worldBottomLeft;

	float nodeDiameter;
	public int gridSizeX, gridSizeY;


	void OnDrawGizmos(){

		if (grid != null && GameObject.FindGameObjectsWithTag ("Player").Length > 0) {
			//there are more than one players - how will this work with the grid?
			List<GameObject> players = new List<GameObject>(GameObject.FindGameObjectsWithTag ("Player"));

			GridNode playerNode = GetNode (players[0].transform.position);

			foreach(GridNode gn in grid){
				Gizmos.color = gn.walkable ? Color.white : Color.red;
				Gizmos.color = (playerNode == gn) ? Color.green : Gizmos.color;

				Gizmos.DrawWireCube(gn.worldPos, new Vector3(nodeDiameter, 1, nodeDiameter));

			}
		}
	}
	// Use this for initialization
	void Start () {
		nodeDiameter = nodeRadius * 2;
		gridSizeX = Mathf.RoundToInt (gridWorldSize.x / nodeDiameter);
		gridSizeY = Mathf.RoundToInt (gridWorldSize.y / nodeDiameter);
		CreateGrid ();

	}
	//Returns the node on the grid that a point in the world corresponds to.
	//currently doesnt take into account the y axis but if we add in slopes etc to our maps later ill fix that.
	public GridNode GetNode(Vector3 worldPos){
		float perX = (worldPos.x + gridWorldSize.x/2) /gridWorldSize.x;
		float perY = (worldPos.z + gridWorldSize.y/2) /gridWorldSize.y;
		perX = Mathf.Clamp01(perX);
		perY = Mathf.Clamp01(perY);

		int x = Mathf.RoundToInt((gridSizeX-1) * perX);
		int y = Mathf.RoundToInt((gridSizeY-1) * perY);
		if (x < gridSizeX && y < gridSizeY) {
			return grid [x, y];
		} else {
			return null;
		}
	}
	public Vector2 GetVector2(Vector3 worldPos){
		float perX = (worldPos.x + gridWorldSize.x/2) /gridWorldSize.x;
		float perY = (worldPos.z + gridWorldSize.y/2) /gridWorldSize.y;
		perX = Mathf.Clamp01(perX);
		perY = Mathf.Clamp01(perY);

		int x = Mathf.RoundToInt((gridSizeX-1) * perX);
		int y = Mathf.RoundToInt((gridSizeY-1) * perY);
		if (x < gridSizeX && y < gridSizeY) {
			return new Vector2 (x, y);
		}
		return new Vector2 ();
	}
	public bool GetWalkable(int x, int y){
		return grid [x, y].walkable;
	}
	void CreateGrid(){
		grid = new GridNode[gridSizeX, gridSizeY];
		worldBottomLeft = transform.position - Vector3.right * (gridWorldSize.x / 2) - Vector3.forward * (gridWorldSize.y / 2);
		for(int x = 0; x < gridSizeX; x++){
			for(int y = 0; y < gridSizeY; y++){
				Vector3 worldPoint = GetWorldPos (x, y);
				bool walkable = !(Physics.CheckBox (worldPoint, new Vector3(nodeRadius-0.1f, .5f, nodeRadius-0.1f), Quaternion.identity, unwalkableMask));
				grid[x, y] = new GridNode(walkable, worldPoint);
				if (gridSizeX / 2 == x && gridSizeY / 2 == y) {
				}
			}
		}
	}
	public Vector3 GetWorldPos(int x, int y){
		return worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);
	}
}
